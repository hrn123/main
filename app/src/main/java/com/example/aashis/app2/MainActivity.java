package com.example.aashis.app2;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

public class MainActivity extends AppCompatActivity {

    Toolbar toolbar;
    TabLayout tabLayout;
    ViewPager viewPager;
    ViewPagerAdapter viewPagerAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        tabLayout= (TabLayout) findViewById(R.id.tab_layout);
        viewPager= (ViewPager) findViewById(R.id.viewpager);
        viewPagerAdapter=new ViewPagerAdapter(getSupportFragmentManager());

        viewPagerAdapter.addfragment(new HomeFragment(),"");
        viewPagerAdapter.addfragment(new RightsInfo(),"");
        viewPagerAdapter.addfragment(new ProbandQueries(),"");
        viewPagerAdapter.addfragment(new NGOFragment(),"");
        viewPagerAdapter.addfragment(new ProfileFragment(),"");


        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.ic_home_white_24dp);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_info_white_24dp);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_question_answer_white_24dp);
        tabLayout.getTabAt(3).setIcon(R.drawable.ic_language_white_24dp);
        tabLayout.getTabAt(4).setIcon(R.drawable.ic_account_circle_white_24dp);


    }


}
